package tetrisgame;

import com.google.gson.Gson;
import model.Board;
import model.Piece;
import model.PieceColor;
import model.Player;
import server.TetrisGameServerSocket;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TetrisGame {

    Player player1;
    Player player2;

    TetrisGameServerSocket socket;

    Board board;
    Piece currentPiece;
    int level = 1;

    Random rnd = new Random();
    Gson gson = new Gson();

    // Multithreading
    ScheduledExecutorService executor;
    ScheduledFuture<?> futureTask;
    private Runnable moveDownRunnable;

    boolean started = false;

    public TetrisGame(Player player1, Player player2, TetrisGameServerSocket tetrisGameServerSocket) {
        this.player1 = player1;
        this.player2 = player2;
        this.socket = tetrisGameServerSocket;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Board getBoard() {
        return board;
    }

    public void startGame() {
        this.started = true;
        this.board = new Board();
        this.board.setLevel(this.level);
        this.currentPiece = new Piece(0, 5, PieceColor.RED);
        this.board.setCurrentPiece(currentPiece);
        this.socket.updateBoard(this);
        setRandomPlayerTurn();

        moveDownRunnable = () -> {
            if (this.started) {
                moveDown();
            }
        };

        executor = Executors.newScheduledThreadPool(1);
        updateLevel();
    }

    public void changeReadInterval(long time) {
        if (time > 0) {
            if (futureTask != null) {
                futureTask.cancel(true);
            }
            futureTask = executor.scheduleAtFixedRate(moveDownRunnable, 3000, time, TimeUnit.MILLISECONDS);
        }
    }

    public void updateLevel() {
        changeReadInterval(1000 / ((long)Math.pow(level, 1.05)));
    }

    private void setRandomPlayerTurn() {
        int random = rnd.nextInt(2);
        if (random == 0) {
            player1.setTurn(true);
        } else if (random == 1) {
            player2.setTurn(true);
        }
        socket.updatePlayers(this);
    }

    public void moveLeft() {
        board.moveLeft();
        this.socket.updateBoard(this);
    }

    public void moveRight() {
        board.moveRight();
        this.socket.updateBoard(this);
    }

    public void moveDown() {
        board.moveDown();

        if (currentPiece.isPlaced()) {
            if (board.checkLostGame()) {
                decideWinner();
                return;
            }
            int addedScore = board.clearFullRows();
            if (addedScore > 0) {
                if (player1.isTurn()) {
                    player1.setCurrentScore(player1.getCurrentScore() + addedScore);
                } else if (player2.isTurn()) {
                    player2.setCurrentScore(player2.getCurrentScore() + addedScore);
                }
                this.level = board.getLevel();
                this.updateLevel();

                // Update scores
                this.socket.updatePlayers(this);
                // Update level
                this.socket.updateLevel(this, this.level);
            }
            switchTurns();
        }

        this.socket.updateBoard(this);
    }

    private void switchTurns() {
        this.player1.setTurn(!this.player1.isTurn());
        this.player2.setTurn(!this.player2.isTurn());

        // Load new piece
        setNewPiece();

        // Update turns
        this.socket.updatePlayers(this);
        this.socket.updateBoard(this);
    }

    private void setNewPiece() {
        if (this.currentPiece.getColor() == PieceColor.RED) {
            currentPiece = new Piece(0, 5, PieceColor.BLUE);
        } else {
            currentPiece = new Piece(0, 5, PieceColor.RED);
        }
        board.setCurrentPiece(currentPiece);
    }

    private void decideWinner() {
        executor.shutdown();
        if (player1.isTurn()) {
            player1.setCurrentScore(player1.getCurrentScore() - 200);
            if (player1.getCurrentScore() < 0) {
                player1.setCurrentScore(0);
            }
        } else if (player2.isTurn()) {
            player2.setCurrentScore(player2.getCurrentScore() - 200);
            if (player2.getCurrentScore() <= 0) {
                player2.setCurrentScore(0);
            }
        }
        if (player1.getCurrentScore() > player2.getCurrentScore()) {
            socket.setWinner(player1);
            socket.setLoser(player2);
        } else if (player2.getCurrentScore() > player1.getCurrentScore()) {
            socket.setWinner(player2);
            socket.setLoser(player1);
        } else {
            socket.setDraw(this);
        }
    }

    public void endGame() {
        this.executor.shutdown();
        this.started = false;
        this.board = null;
        this.player1 = null;
        this.player2 = null;
        this.level = 1;
    }
}
