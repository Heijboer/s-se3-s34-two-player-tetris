package server;

import com.google.gson.Gson;
import model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.messaging.TetrisClientSocketMessage;
import server.messaging.TetrisServerSocketMessage;
import server.messaging.TetrisSocketServerOperation;
import tetrisgame.TetrisGame;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

@ServerEndpoint(value = "/tetrisgame/")
public class TetrisGameServerSocket {

    Logger logger = LoggerFactory.getLogger(TetrisGameServerSocket.class);

    Random random = new Random();
    Gson gson = new Gson();

    static HashSet<Session> playerSessions = new HashSet<>();
    static HashSet<TetrisGame> tetrisGames = new HashSet<>();
    static List<Player> queue = new ArrayList<>();

    @OnOpen
    public void onConnect(Session session) {
        logger.info("[Connected] SessionID: {}", session.getId());
        String message = String.format("[New client with client side session ID]: %s", session.getId());
        broadcast(message);
        playerSessions.add(session);
        logger.info("[#sessions]: {}", playerSessions.size());
    }

    @OnMessage
    public void onText(String message, Session session) {
        logger.info("[Session ID] : {} [Received] : {}", session.getId(), message);
        handleMessage(message, session);
    }

    @OnClose
    public void onClose(CloseReason reason, Session session) {
        logger.info("[Session ID] : {} [Socket Closed:] : {}", session.getId(), reason);
        TetrisGame game = findGameSession(session);
        if (game != null) {
            playerSessions.remove(game.getPlayer1().getSession());
            playerSessions.remove(game.getPlayer2().getSession());
            tetrisGames.remove(game);
            game.endGame();
            game = null;
        }
    }

    @OnError
    public void onError(Throwable cause, Session session) {
        logger.info("[Session ID] : {} [ERROR] : {}", session.getId(), cause.getMessage());
        logger.error(cause.getMessage());
    }

    public void broadcast(String s) {
        logger.info("[Broadcast] { {} } to:", s);
        for (Session session : playerSessions) {
            try {
                session.getBasicRemote().sendText(s);
                logger.info("\t\t >> Client associated with server side session ID: {}", session.getId());
            } catch (IOException e) {
                logger.info(e.getMessage());
            }
        }
        logger.info("[End of Broadcast]");
    }

    private void queuePlayer(Player player) {
        queue.add(player);

        if (queue.size() >= 2) {
            Player player1 = queue.get(random.nextInt(queue.size()));
            queue.remove(player1);
            Player player2 = queue.get(random.nextInt(queue.size()));
            queue.remove(player2);

            TetrisGame tetrisGame = new TetrisGame(player1, player2, this);
            tetrisGames.add(tetrisGame);
            logger.info("[New Game Session] : Player 1: {} Player 2: {}", player1.getUsername(), player2.getUsername());
            setPlayers(tetrisGame);
        }
    }

    private void handleMessage(String message, Session session) {

        TetrisClientSocketMessage wMessage = gson.fromJson(message, TetrisClientSocketMessage.class);
        TetrisGame game = findGameSession(session);

        switch (wMessage.getOperation()) {
            case REGISTER:
                Player player = gson.fromJson(wMessage.getContent(), Player.class);
                player.setSession(session);
                logger.info("[REGISTERED PLAYER] : {}", player.getUsername());
                queuePlayer(player);
                break;
            case MOVELEFT:
                if (game != null) {
                    game.moveLeft();
                }
                break;
            case MOVERIGHT:
                if (game != null) {
                    game.moveRight();
                }
                break;
            case MOVEDOWN:
                if (game != null) {
                    game.moveDown();
                }
                break;
            case ENDGAME:
                if (game != null) {
                    playerSessions.remove(game.getPlayer1().getSession());
                    playerSessions.remove(game.getPlayer2().getSession());
                    tetrisGames.remove(game);
                    game.endGame();
                    game = null;
                }
                break;
            default:
                logger.error("Invalid operation");
        }
    }

    private TetrisGame findGameSession(Session session) {
        for (TetrisGame game : tetrisGames) {
            if (game.getPlayer1().getSession().getId().equals(session.getId()) || game.getPlayer2().getSession().getId().equals(session.getId())) {
                return game;
            }
        }
        return null;
    }

    public void setPlayers(TetrisGame tetrisGame) {
        Session player1Session = tetrisGame.getPlayer1().getSession();
        Session player2Session = tetrisGame.getPlayer2().getSession();

        sendSocketMessage(tetrisGame.getPlayer1(), TetrisSocketServerOperation.SETPLAYER, player1Session);
        sendSocketMessage(tetrisGame.getPlayer2(), TetrisSocketServerOperation.SETOPPONENT, player1Session);

        sendSocketMessage(tetrisGame.getPlayer2(), TetrisSocketServerOperation.SETPLAYER, player2Session);
        sendSocketMessage(tetrisGame.getPlayer1(), TetrisSocketServerOperation.SETOPPONENT, player2Session);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ie) {
            logger.info(ie.getMessage());
            Thread.currentThread().interrupt();
        }

        tetrisGame.startGame();
    }

    public void updatePlayers(TetrisGame tetrisGame) {
        Session player1Session = tetrisGame.getPlayer1().getSession();
        Session player2Session = tetrisGame.getPlayer2().getSession();

        sendSocketMessage(tetrisGame.getPlayer1(), TetrisSocketServerOperation.UPDATEPLAYER, player1Session);
        sendSocketMessage(tetrisGame.getPlayer2(), TetrisSocketServerOperation.UPDATEOPPONENT, player1Session);

        sendSocketMessage(tetrisGame.getPlayer2(), TetrisSocketServerOperation.UPDATEPLAYER, player2Session);
        sendSocketMessage(tetrisGame.getPlayer1(), TetrisSocketServerOperation.UPDATEOPPONENT, player2Session);
    }

    public void updateBoard(TetrisGame tetrisGame) {

        Session player1Session = tetrisGame.getPlayer2().getSession();
        Session player2Session = tetrisGame.getPlayer1().getSession();

        sendSocketMessage(tetrisGame.getBoard(), TetrisSocketServerOperation.UPDATEBOARD, player1Session);
        sendSocketMessage(tetrisGame.getBoard(), TetrisSocketServerOperation.UPDATEBOARD, player2Session);
    }

    public void updateLevel(TetrisGame tetrisGame, int level) {

        Session player1Session = tetrisGame.getPlayer2().getSession();
        Session player2Session = tetrisGame.getPlayer1().getSession();

        sendSocketMessage(level, TetrisSocketServerOperation.UPDATELEVEL, player1Session);
        sendSocketMessage(level, TetrisSocketServerOperation.UPDATELEVEL, player2Session);
    }

    public void setWinner(Player winningPlayer) {

        Session winningPlayerSession = winningPlayer.getSession();

        sendSocketMessage(winningPlayer, TetrisSocketServerOperation.SETWINNER, winningPlayerSession);
    }

    public void setLoser(Player losingPlayer) {
        Session losingPlayerSession = losingPlayer.getSession();

        sendSocketMessage(losingPlayer, TetrisSocketServerOperation.SETLOSER, losingPlayerSession);
    }

    public void setDraw(TetrisGame tetrisGame) {

        Session player1Session = tetrisGame.getPlayer2().getSession();
        Session player2Session = tetrisGame.getPlayer1().getSession();

        sendSocketMessage(null, TetrisSocketServerOperation.SETDRAW, player1Session);
        sendSocketMessage(null, TetrisSocketServerOperation.SETDRAW, player2Session);
    }

    private void sendSocketMessage(Object object, TetrisSocketServerOperation operation, Session session) {
        try {
            TetrisServerSocketMessage wsMessage = new TetrisServerSocketMessage();
            wsMessage.setOperation(operation);
            wsMessage.setContent(gson.toJson(object));
            session.getBasicRemote().sendText(gson.toJson(wsMessage));
        } catch (IOException e) {
            logger.info(e.getMessage());
        }
    }
}
