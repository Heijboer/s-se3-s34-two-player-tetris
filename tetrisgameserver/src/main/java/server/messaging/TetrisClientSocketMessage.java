package server.messaging;

public class TetrisClientSocketMessage {

    private TetrisSocketClientOperation operation;

    private String content;

    public TetrisSocketClientOperation getOperation() {
        return operation;
    }

    public void setOperation(TetrisSocketClientOperation operation) {
        this.operation = operation;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
