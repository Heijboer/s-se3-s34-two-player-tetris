package server.messaging;

public enum  TetrisSocketClientOperation {
    REGISTER,
    MOVELEFT,
    MOVERIGHT,
    MOVEDOWN,
    ENDGAME
}
