package server.messaging;

public class TetrisServerSocketMessage {

    private TetrisSocketServerOperation operation;

    private String content;

    public TetrisSocketServerOperation getOperation() {
        return operation;
    }

    public void setOperation(TetrisSocketServerOperation operation) {
        this.operation = operation;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
