package model;

import junit.framework.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {


    private Board board;

    @BeforeEach
    void setUp() {
        board = new Board();
    }

    @AfterEach
    void tearDown() {
    }

    @Test()
    void testMovePieceDown() {
        Piece piece = new Piece(0, 5);

        board.setCurrentPiece(piece);

        Assert.assertTrue(board.canCurrentPieceMoveDown());
    }

    @Test()
    void testMovePieceDownBottom() {
        Piece piece = new Piece(0, 5);

        board.setCurrentPiece(piece);

        Assert.assertTrue(board.canCurrentPieceMoveDown());

        // Move current piece to bottom of the board
        for (int i = 0; i < 25; i++) {
            board.moveDown();
        }

        Assert.assertFalse(board.canCurrentPieceMoveDown());
    }

    @Test()
    void testMovePieceDownBlocked() {
        Piece blockingPiece = new Piece(15, 5);
        Piece piece = new Piece(0, 5);

        board.setPiece(blockingPiece);
        board.setCurrentPiece(piece);

        board.printBoard();

        System.out.println("\n");

        Assert.assertTrue(board.canCurrentPieceMoveDown());

        // Move current piece to bottom of the board
        for (int i = 0; i < 25; i++) {
            board.moveDown();
        }

        board.printBoard();

        Assert.assertFalse(board.canCurrentPieceMoveDown());
    }

    @Test()
    void testMovePieceLeft() {
        Piece piece = new Piece(0, 5);

        board.setCurrentPiece(piece);

        board.moveLeft();

        Assert.assertTrue(board.canCurrentPieceMoveLeft());
    }

    @Test()
    void testMovePieceLeftSide() {
        Piece piece = new Piece(0, 5);

        board.setCurrentPiece(piece);

        Assert.assertTrue(board.canCurrentPieceMoveLeft());

        // Move current piece to bottom of the board
        for (int i = 0; i < 25; i++) {
            board.moveLeft();
        }

        Assert.assertFalse(board.canCurrentPieceMoveLeft());
    }

    @Test()
    void testMovePieceLeftBlocked() {
        Piece blockingPiece = new Piece(0, 2);
        Piece piece = new Piece(0, 5);

        board.setPiece(blockingPiece);
        board.setCurrentPiece(piece);

        board.printBoard();

        System.out.println("\n");

        Assert.assertTrue(board.canCurrentPieceMoveLeft());

        // Move current piece to bottom of the board
        for (int i = 0; i < 5; i++) {
            board.moveLeft();
        }

        board.printBoard();

        Assert.assertFalse(board.canCurrentPieceMoveLeft());
    }

    @Test()
    void testMovePieceRight() {
        Piece piece = new Piece(0, 5);

        board.setCurrentPiece(piece);

        board.moveRight();

        Assert.assertTrue(board.canCurrentPieceMoveRight());
    }

    @Test()
    void testMovePieceRightSide() {
        Piece piece = new Piece(0, 5);

        board.setCurrentPiece(piece);

        Assert.assertTrue(board.canCurrentPieceMoveRight());

        // Move current piece to bottom of the board
        for (int i = 0; i < 25; i++) {
            board.moveRight();
        }

        Assert.assertFalse(board.canCurrentPieceMoveRight());
    }

    @Test()
    void testMovePieceRightBlocked() {
        Piece blockingPiece = new Piece(0, 9);
        Piece piece = new Piece(0, 5);

        board.setPiece(blockingPiece);
        board.setCurrentPiece(piece);

        board.printBoard();

        System.out.println("\n");

        Assert.assertTrue(board.canCurrentPieceMoveRight());

        // Move current piece to bottom of the board
        for (int i = 0; i < 10; i++) {
            board.moveRight();
        }

        board.printBoard();

        Assert.assertFalse(board.canCurrentPieceMoveRight());
    }

    @Test()
    void testMoveAllPiecesDown() {

        board.setPiece(new Piece(19, 0));
        board.setPiece(new Piece(18, 0));
        board.setPiece(new Piece(17, 0));
        board.setPiece(new Piece(19, 1));
        board.setPiece(new Piece(19, 2));
        board.setPiece(new Piece(19, 3));
        board.setPiece(new Piece(18, 3));
        board.setPiece(new Piece(17, 3));
        board.setPiece(new Piece(19, 4));
        board.setPiece(new Piece(18, 4));
        board.setPiece(new Piece(19, 5));
        board.setPiece(new Piece(19, 6));
        board.setPiece(new Piece(19, 7));
        board.setPiece(new Piece(19, 8));

        board.printBoard();
        System.out.println("\n");

        // Adding last piece which should allow for removal of row
        board.setCurrentPiece();

        for (int i = 0; i < 5; i++){
            board.moveRight();
        }

        for (int i = 0; i < 19; i++){
            board.moveDown();
        }

        board.clearFullRows();

        board.printBoard();

        Assert.assertNull(board.getCells()[18][4]);
        Assert.assertNotNull(board.getCells()[19][4]);
    }

    @Test()
    void testClearFullRows() {

        board.setPiece(new Piece(19, 0));
        board.setPiece(new Piece(19, 1));
        board.setPiece(new Piece(19, 2));
        board.setPiece(new Piece(19, 3));
        board.setPiece(new Piece(19, 4));
        board.setPiece(new Piece(19, 5));
        board.setPiece(new Piece(19, 6));
        board.setPiece(new Piece(19, 7));
        board.setPiece(new Piece(19, 8));

        board.printBoard();
        System.out.println("\n");

        board.clearFullRows();

        boolean notNull = false;
        for(Piece[] array : board.getCells()){
            for(Piece piece : array){
                if(piece!=null){
                    notNull=true;
                    break;
                }
            }
        }

        Assert.assertTrue(notNull);

        // Adding last piece which should allow for removal of row

        board.setCurrentPiece();

        for (int i = 0; i < 5; i++){
            board.moveRight();
        }

        for (int i = 0; i < 19; i++){
            board.moveDown();
        }

        board.clearFullRows();

        board.printBoard();

        notNull = false;

        for(Piece[] array : board.getCells()){
            for(Piece piece : array){
                if(piece!=null){
                    notNull=true;
                    break;
                }
            }
        }

        Assert.assertFalse(notNull);
    }

    @Test()
    void testClearFullRowsOneRow() {

        // Row one
        board.setPiece(new Piece(19, 0));
        board.setPiece(new Piece(19, 1));
        board.setPiece(new Piece(19, 2));
        board.setPiece(new Piece(19, 3));
        board.setPiece(new Piece(19, 4));
        board.setPiece(new Piece(19, 5));
        board.setPiece(new Piece(19, 6));
        board.setPiece(new Piece(19, 7));
        board.setPiece(new Piece(19, 8));
        board.setPiece(new Piece(19, 9));

        board.printBoard();
        System.out.println("\n");

        int score = board.clearFullRows();

        Assert.assertEquals(40, score);

        board.printBoard();

        boolean notNull = false;

        for(Piece[] array : board.getCells()){
            for(Piece piece : array){
                if(piece!=null){
                    notNull=true;
                    break;
                }
            }
        }

        Assert.assertFalse(notNull);
    }

    @Test()
    void testClearFullRowsTwoRows() {

        // First row
        board.setPiece(new Piece(19, 0));
        board.setPiece(new Piece(19, 1));
        board.setPiece(new Piece(19, 2));
        board.setPiece(new Piece(19, 3));
        board.setPiece(new Piece(19, 4));
        board.setPiece(new Piece(19, 5));
        board.setPiece(new Piece(19, 6));
        board.setPiece(new Piece(19, 7));
        board.setPiece(new Piece(19, 8));
        board.setPiece(new Piece(19, 9));
        // Second row
        board.setPiece(new Piece(18, 0));
        board.setPiece(new Piece(18, 1));
        board.setPiece(new Piece(18, 2));
        board.setPiece(new Piece(18, 3));
        board.setPiece(new Piece(18, 4));
        board.setPiece(new Piece(18, 5));
        board.setPiece(new Piece(18, 6));
        board.setPiece(new Piece(18, 7));
        board.setPiece(new Piece(18, 8));
        board.setPiece(new Piece(18, 9));

        board.printBoard();
        System.out.println("\n");

        int score = board.clearFullRows();

        Assert.assertEquals(200, score);

        board.printBoard();

        boolean notNull = false;

        for(Piece[] array : board.getCells()){
            for(Piece piece : array){
                if(piece!=null){
                    notNull=true;
                    break;
                }
            }
        }

        Assert.assertFalse(notNull);
    }

    @Test()
    void testClearFullRowsThreeRows() {

        // First row
        board.setPiece(new Piece(19, 0));
        board.setPiece(new Piece(19, 1));
        board.setPiece(new Piece(19, 2));
        board.setPiece(new Piece(19, 3));
        board.setPiece(new Piece(19, 4));
        board.setPiece(new Piece(19, 5));
        board.setPiece(new Piece(19, 6));
        board.setPiece(new Piece(19, 7));
        board.setPiece(new Piece(19, 8));
        board.setPiece(new Piece(19, 9));
        // Second row
        board.setPiece(new Piece(18, 0));
        board.setPiece(new Piece(18, 1));
        board.setPiece(new Piece(18, 2));
        board.setPiece(new Piece(18, 3));
        board.setPiece(new Piece(18, 4));
        board.setPiece(new Piece(18, 5));
        board.setPiece(new Piece(18, 6));
        board.setPiece(new Piece(18, 7));
        board.setPiece(new Piece(18, 8));
        board.setPiece(new Piece(18, 9));
        // Third row
        board.setPiece(new Piece(17, 0));
        board.setPiece(new Piece(17, 1));
        board.setPiece(new Piece(17, 2));
        board.setPiece(new Piece(17, 3));
        board.setPiece(new Piece(17, 4));
        board.setPiece(new Piece(17, 5));
        board.setPiece(new Piece(17, 6));
        board.setPiece(new Piece(17, 7));
        board.setPiece(new Piece(17, 8));
        board.setPiece(new Piece(17, 9));

        board.printBoard();
        System.out.println("\n");

        int score = board.clearFullRows();

        Assert.assertEquals(900, score);

        board.printBoard();

        boolean notNull = false;

        for(Piece[] array : board.getCells()){
            for(Piece piece : array){
                if(piece!=null){
                    notNull=true;
                    break;
                }
            }
        }

        Assert.assertFalse(notNull);
    }

    @Test()
    void testClearFullRowsFourRows() {

        // First row
        board.setPiece(new Piece(19, 0));
        board.setPiece(new Piece(19, 1));
        board.setPiece(new Piece(19, 2));
        board.setPiece(new Piece(19, 3));
        board.setPiece(new Piece(19, 4));
        board.setPiece(new Piece(19, 5));
        board.setPiece(new Piece(19, 6));
        board.setPiece(new Piece(19, 7));
        board.setPiece(new Piece(19, 8));
        board.setPiece(new Piece(19, 9));
        // Second row
        board.setPiece(new Piece(18, 0));
        board.setPiece(new Piece(18, 1));
        board.setPiece(new Piece(18, 2));
        board.setPiece(new Piece(18, 3));
        board.setPiece(new Piece(18, 4));
        board.setPiece(new Piece(18, 5));
        board.setPiece(new Piece(18, 6));
        board.setPiece(new Piece(18, 7));
        board.setPiece(new Piece(18, 8));
        board.setPiece(new Piece(18, 9));
        // Third row
        board.setPiece(new Piece(17, 0));
        board.setPiece(new Piece(17, 1));
        board.setPiece(new Piece(17, 2));
        board.setPiece(new Piece(17, 3));
        board.setPiece(new Piece(17, 4));
        board.setPiece(new Piece(17, 5));
        board.setPiece(new Piece(17, 6));
        board.setPiece(new Piece(17, 7));
        board.setPiece(new Piece(17, 8));
        board.setPiece(new Piece(17, 9));
        // Fourth row
        board.setPiece(new Piece(16, 0));
        board.setPiece(new Piece(16, 1));
        board.setPiece(new Piece(16, 2));
        board.setPiece(new Piece(16, 3));
        board.setPiece(new Piece(16, 4));
        board.setPiece(new Piece(16, 5));
        board.setPiece(new Piece(16, 6));
        board.setPiece(new Piece(16, 7));
        board.setPiece(new Piece(16, 8));
        board.setPiece(new Piece(16, 9));

        board.printBoard();
        System.out.println("\n");

        int score = board.clearFullRows();

        Assert.assertEquals(3200, score);

        board.printBoard();

        boolean notNull = false;

        for(Piece[] array : board.getCells()){
            for(Piece piece : array){
                if(piece!=null){
                    notNull=true;
                    break;
                }
            }
        }

        Assert.assertFalse(notNull);
    }
}