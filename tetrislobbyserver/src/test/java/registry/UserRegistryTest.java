package registry;

import data.interfaces.IUserContext;
import data.memorycontext.UserMemoryContext;
import model.User;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UserRegistryTest {

    IUserContext userContext;
    UserRegistry userRegistry;

    @BeforeEach
    void setUp() {
        userContext = new UserMemoryContext();
        userRegistry = new UserRegistry(userContext);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void registerUser() {
        // Assert user was added
        assertThat(userContext.getAllUsers(), IsEmptyCollection.empty());

        String username = "John";
        String password = "SecurePassword";
        userRegistry.registerUser(username, password);

        // Assert user was added
        assertThat("Successfully registered", userContext.getAllUsers(), not(IsEmptyCollection.empty()));
    }

    @Test
    void login() {
        String username = "Username";
        String password = "Password";
        userRegistry.registerUser(username, password);

        User loggedInUser = userRegistry.login(username, password);
        boolean loginCheck = false;

        if (loggedInUser != null && loggedInUser.getUsername().equalsIgnoreCase(username) && password.equals(password)) {
            loginCheck = true;
        }

        Assert.assertTrue("User Successfully logged in", loginCheck);
    }
}