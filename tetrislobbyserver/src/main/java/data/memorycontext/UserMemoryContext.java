package data.memorycontext;

import data.interfaces.IUserContext;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class UserMemoryContext implements IUserContext {

    int id = 0;
    List<User> users = new ArrayList<>();

    @Override
    public void addUser(String username, String password, int highscore) {
        users.add(new User(id, username, password, highscore));
        incrementId();
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public User getUserById(int userId) {
        for (User user : users) {
            if (user.getUserId() == userId){
                return user;
            }
        }
        return null;
    }

    @Override
    public void updateHighscore(int userId, int highscore) {
        getUserById(userId).setHighscore(highscore);
    }

    @Override
    public int getHighscore(int userId) {
        return getUserById(userId).getHighscore();
    }

    @Override
    public List<User> getLeaderboard() {
        //TODO: getLeaderBoard() implementation
        return new ArrayList<>();
    }

    private void incrementId() {
        id++;
    }
}
