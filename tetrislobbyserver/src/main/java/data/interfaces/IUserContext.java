package data.interfaces;

import model.User;

import java.util.List;

public interface IUserContext {

    void addUser(String username, String password, int highscore);

    List<User> getAllUsers();

    User getUserById(int userId);

    void updateHighscore(int userId, int highscore);

    int getHighscore(int userId);

    List<User> getLeaderboard();
}
