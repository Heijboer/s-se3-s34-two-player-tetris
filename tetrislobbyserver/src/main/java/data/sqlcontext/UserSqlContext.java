package data.sqlcontext;

import data.connection.DatabaseConnection;
import data.interfaces.IUserContext;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserSqlContext implements IUserContext {

    Logger logger = LoggerFactory.getLogger(UserSqlContext.class);

    public void addUser(String username, String password, int highscore) {

        try (Connection con = DatabaseConnection.getConnection();
             PreparedStatement st = con.prepareStatement("INSERT INTO user(username, password, highscore) VALUES (?,?,?)")) {

            st.setString(1, username);
            st.setString(2, password);
            st.setInt(3, highscore);
            st.executeUpdate();

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        try (Connection con = DatabaseConnection.getConnection();
             Statement st = con.createStatement()){
            // our SQL SELECT query.
            // if you only need a few columns, specify them by name instead of using "*"
            String query = "SELECT * FROM user";

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next()) {
                int id = rs.getInt("userId");
                String username = rs.getString("username");
                String password = rs.getString("password");
                int highscore = rs.getInt("highscore");

                User user = new User(id, username, password, highscore);
                users.add(user);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
        return users;
    }


    @Override
    public User getUserById(int userId) {
        return null;
    }

    @Override
    public void updateHighscore(int userId, int highscore) {
        // TODO: highscore implementation
    }

    @Override
    public int getHighscore(int userId) {
        return 0;
    }

    @Override
    public List<User> getLeaderboard() {
        return new ArrayList<>();
    }
}