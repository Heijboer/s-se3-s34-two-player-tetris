package data.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {

    private DatabaseConnection() {
        // Nothing
    }

    private static final Logger logger = LoggerFactory.getLogger(DatabaseConnection.class);

    private static Connection con;

    public static Connection getConnection() {

        Properties prop = new Properties();

        try {
            InputStream is = new FileInputStream("dbconfig.properties");
            prop.load(is);
        } catch (IOException e) {
            logger.info(e.getMessage());
        }

        try {
            Class.forName(prop.getProperty("db.driver"));
            con = DriverManager.getConnection(prop.getProperty("db.url"), prop.getProperty("db.username"), prop.getProperty("db.password"));
            // log an exception. fro example:
            logger.info("Failed to create the database connection.");
        } catch (ClassNotFoundException | SQLException ex) {
            // log an exception. for example:
            logger.info(ex.getMessage());
            logger.info("Driver not found.");
        }
        return con;
    }
}
