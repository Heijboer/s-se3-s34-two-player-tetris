package data.repositories;

import data.interfaces.IUserContext;
import model.User;

import java.util.List;

public class UserRepository {

    IUserContext userContext;

    public UserRepository(IUserContext userContext) {
        this.userContext = userContext;
    }

    public void addUser(String username, String password, int highscore) {
        this.userContext.addUser(username, password, highscore);
    }

    public List<User> getAllUsers() {
        return this.userContext.getAllUsers();
    }

    public User getUserById(int userId) {
        return this.userContext.getUserById(userId);
    }

    public void updateHighscore(int userId, int highscore) {
        this.userContext.updateHighscore(userId, highscore);
    }

    public int getHighscore(int userId) {
        return this.userContext.getHighscore(userId);
    }

    public List<User> getLeaderboard() {
        return this.userContext.getLeaderboard();
    }
}
