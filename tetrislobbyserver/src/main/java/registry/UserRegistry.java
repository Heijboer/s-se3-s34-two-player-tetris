package registry;

import data.interfaces.IUserContext;
import data.repositories.UserRepository;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PasswordEncrypter;

import java.util.List;

public class UserRegistry {

    private static final Logger logger = LoggerFactory.getLogger(UserRegistry.class);

    UserRepository userRepository;
    PasswordEncrypter encrypter;

    public UserRegistry(IUserContext userContext) {
        userRepository = new UserRepository(userContext);
        encrypter = new PasswordEncrypter();
    }

    public boolean registerUser(String username, String password) {
        if (checkUsernameAvailable(username)){
            userRepository.addUser(username, encrypter.encode(password), 0);
            logger.info("User registered: {}", username);
            return true;
        } else {
            return false;
        }
    }

    private boolean checkUsernameAvailable(String username) {
        List<User> users;
        users = userRepository.getAllUsers();

        for (User user : users) {
            if (username.equalsIgnoreCase(user.getUsername())){
                return false;
            }
        }
        return true;
    }

    public User login(String username, String password) {
        List<User> users;
        users = userRepository.getAllUsers();

        for (User user : users) {
            if (username.equalsIgnoreCase(user.getUsername()) && password.equals(encrypter.decode(user.getPassword()))){
                return user;
            }
        }
        return null;
    }
}
