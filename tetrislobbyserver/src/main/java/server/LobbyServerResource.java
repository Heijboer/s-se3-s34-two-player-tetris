package server;

import data.sqlcontext.UserSqlContext;
import model.User;
import pojo.UserRegisterPojo;
import registry.UserRegistry;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tetrislobby")
public class LobbyServerResource {

    UserRegistry registry;

    public LobbyServerResource() {
        this.registry = new UserRegistry(new UserSqlContext());
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response registerUser(UserRegisterPojo pojo) {
        String result = Boolean.toString(registry.registerUser(pojo.getUsername(), pojo.getPassword()));
        return Response.status(201).entity(result).build();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User login(UserRegisterPojo pojo) {
        return registry.login(pojo.getUsername(), pojo.getPassword());
    }
}
