package model;

public class User {

    private int userId;
    private String username;
    private String password;
    private int highscore;

    public User(int id, String username, String password, int highscore) {
        this.userId = id;
        this.username = username;
        this.password = password;
        this.highscore = highscore;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getHighscore() {
        return highscore;
    }

    public void setHighscore(int highscore) {
        this.highscore = highscore;
    }
}
