package utils;

import org.apache.commons.codec.binary.Base64;

public class PasswordEncrypter {

    public String encode(String passwordToEncode) {
        byte[] bytesEncoded = Base64.encodeBase64(passwordToEncode.getBytes());
        return new String(bytesEncoded);
    }

    public String decode(String passwordToDecode) {
        byte[] valueDecoded = Base64.decodeBase64(passwordToDecode.getBytes());
        return new String(valueDecoded);
    }
}
