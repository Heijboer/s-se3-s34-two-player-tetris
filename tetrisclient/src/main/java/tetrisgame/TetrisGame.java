package tetrisgame;

import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import networking.TetrisGameClientSocket;
import networking.messaging.TetrisServerSocketMessage;
import networking.messaging.TetrisSocketClientOperation;
import networking.messaging.TetrisClientSocketMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tetrisgame.model.Board;
import tetrisgame.model.Piece;
import tetrisgame.model.PieceColor;
import tetrisgame.model.Player;
import tetrisgui.ITetrisGUI;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.Random;

public class TetrisGame implements ITetrisGame {

    Logger logger = LoggerFactory.getLogger(TetrisGame.class);

    ITetrisGUI application;
    Board board;
    Piece currentPiece;
    Player player;
    Player opponent;
    int level = 1;
    boolean singleplayer;
    private Random rnd = new Random();
    private boolean started = false;
    Gson gson = new Gson();

    private static final URI uri = URI.create("ws://localhost:8095/tetrisgame/");
    private Session session;

    public void registerPlayer(String name) {
        this.player = new Player(name);
    }

    public void startNewGame(ITetrisGUI application, boolean singleplayer) {
        this.application = application;
        this.singleplayer = singleplayer;
        if (singleplayer) {
            this.started = true;
            opponent = new Player("computer");
            application.setOpponent(opponent);
            application.setPlayer(player);
            application.setScoreBoard();
            player.setTurn(true);
            board = new Board();
            board.setLevel(this.level);
            currentPiece = new Piece(0, 5, PieceColor.RED);
            board.setCurrentPiece(currentPiece);
            application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
            application.updateLevel(this.level);
            player.setTurn(true);
        } else {
            this.started = true;
            try {
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                session = container.connectToServer(TetrisGameClientSocket.class, uri);
                sendSocketMessage(this.player, TetrisSocketClientOperation.REGISTER);

                // add listener
                TetrisGameClientSocket.addMessageHandler(this::handleMessage);

            } catch (DeploymentException | IOException e) {
                logger.info(e.getMessage());
            }

        }
    }

    public void handleMessage(String message) {

        TetrisServerSocketMessage wMessage = gson.fromJson(message, TetrisServerSocketMessage.class);

        switch (wMessage.getOperation()) {
            case SETPLAYER:
                Player newPlayer = gson.fromJson(wMessage.getContent(), Player.class);
                logger.info("Player : {}", newPlayer.getUsername());
                this.player = newPlayer;

                Platform.runLater(() -> {
                    application.setPlayer(newPlayer);
                    application.setScoreBoard();
                    application.updateLevel(level);
                });
                break;
            case SETOPPONENT:
                Player newOpponent = gson.fromJson(wMessage.getContent(), Player.class);
                logger.info("Opponent : {}", newOpponent.getUsername());
                this.opponent = newOpponent;

                Platform.runLater(() -> application.setOpponent(newOpponent));
                break;
            case UPDATEPLAYER:
                Player updatedPlayer = gson.fromJson(wMessage.getContent(), Player.class);
                this.player.setTurn(updatedPlayer.isTurn());
                this.player.setCurrentScore(updatedPlayer.getCurrentScore());

                Platform.runLater(() -> {
                    application.updatePlayer(player);
                    application.setScoreBoard();
                    application.updateLevel(level);
                });
                break;
            case UPDATEOPPONENT:
                Player updatedOpponent = gson.fromJson(wMessage.getContent(), Player.class);
                this.opponent.setTurn(updatedOpponent.isTurn());
                this.opponent.setCurrentScore(updatedOpponent.getCurrentScore());

                Platform.runLater(() -> {
                    application.updateOpponent(opponent);
                    application.setScoreBoard();
                    application.updateLevel(level);
                });
                break;
            case UPDATEBOARD:
                this.board = gson.fromJson(wMessage.getContent(), Board.class);
                updateWholeBoard(this.board.getCells());
                break;
            case UPDATELEVEL:
                int updatedLevel = gson.fromJson(wMessage.getContent(), int.class);
                this.level = updatedLevel;
                Platform.runLater(() -> application.updateLevel(level));
                break;
            case SETWINNER:
                Platform.runLater(() -> application.setWon(player.getUsername()));
                break;
            case SETLOSER:
                Platform.runLater(() -> application.setWon(opponent.getUsername()));
                break;
            case SETDRAW:
                Platform.runLater(() -> application.setDraw());
                break;
        }
    }

    public void moveDown() {
        if (checkPlayerTurn()) {
            if (singleplayer) {
                if (board.moveDown()) {
                    application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
                    application.changeSquareState(currentPiece.getX(), currentPiece.getY() - 1, false);
                }
                if (currentPiece.isPlaced()) {
                    if (board.checkLostGame()) {
                        decideWinner();
                        return;
                    }
                    int addedScore = board.clearFullRows();
                    if (addedScore > 0) {
                        player.setCurrentScore(player.getCurrentScore() + addedScore);
                        updateWholeBoard(board.getCells());
                        this.level = board.getLevel();
                        application.setScoreBoard();
                        application.updateLevel(level);
                    }
                    switchTurns();
                }
            } else {
                sendSocketMessage(null, TetrisSocketClientOperation.MOVEDOWN);
            }
        }
    }

    public void moveLeft() {
        if (player.isTurn()) {
            if (singleplayer) {
                if (board.moveLeft()) {
                    application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
                    application.changeSquareState(currentPiece.getX() + 1, currentPiece.getY(), false);
                }
            } else {
                sendSocketMessage(null, TetrisSocketClientOperation.MOVELEFT);
            }
        } else {
            application.alertError("Its " + opponent.getUsername() + "'s " + "turn");
        }
    }

    public void moveRight() {
        if (player.isTurn()) {
            if (singleplayer) {
                if (board.moveRight()) {
                    application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
                    application.changeSquareState(currentPiece.getX() - 1, currentPiece.getY(), false);
                }
            } else {
                sendSocketMessage(null, TetrisSocketClientOperation.MOVERIGHT);
            }
        } else {
            application.alertError("Its " + opponent.getUsername() + "'s " + "turn");
        }

    }

    private void decideWinner() {
        if (player.isTurn()) {
            player.setCurrentScore(player.getCurrentScore() - 200);
            if (player.getCurrentScore() < 0) {
                player.setCurrentScore(0);
            }
        } else if (opponent.isTurn()) {
            opponent.setCurrentScore(opponent.getCurrentScore() - 200);
            if (opponent.getCurrentScore() <= 0) {
                opponent.setCurrentScore(0);
            }
        }
        if (player.getCurrentScore() > opponent.getCurrentScore()) {
            application.setWon(player.getUsername());
        } else if (opponent.getCurrentScore() > player.getCurrentScore()) {
            application.setWon(opponent.getUsername());
        } else {
            application.setDraw();
        }
    }

    public void switchTurns() {
        if (singleplayer) {
            this.player.setTurn(!this.player.isTurn());
            this.opponent.setTurn(!this.opponent.isTurn());

            // Load new piece
            if (this.opponent.isTurn()) {
                currentPiece = new Piece(0, 5, PieceColor.BLUE);
            } else if (this.player.isTurn()) {
                currentPiece = new Piece(0, 5, PieceColor.RED);
            }
            board.setCurrentPiece(currentPiece);
            application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);

            if (opponent.isTurn()) {
                playComputerMove();
            }
        }
    }

    private void playComputerMove() {
        for (int i = 0; i < 15; i++) {
            int random = rnd.nextInt(2);
            if (random == 1 && board.moveRight()) {
                application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
                application.changeSquareState(currentPiece.getX() - 1, currentPiece.getY(), false);
            } else if (random == 0 && board.moveLeft()) {
                application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
                application.changeSquareState(currentPiece.getX() + 1, currentPiece.getY(), false);
            }
        }
        for (int i = 0; i < 20; i++) {
            if (board.moveDown()) {
                application.changeSquareState(currentPiece.getX(), currentPiece.getY(), true);
                application.changeSquareState(currentPiece.getX(), currentPiece.getY() - 1, false);
            }
            if (currentPiece.isPlaced()) {
                opponent.setCurrentScore(opponent.getCurrentScore() + board.clearFullRows());
                updateWholeBoard(board.getCells());
                application.setScoreBoard();
            }
        }
        switchTurns();
    }

    private void updateWholeBoard(Piece[][] board) {
        application.clearBoard();

        for (Piece[] row : board) {
            for (Piece piece : row) {
                if (piece != null) {
                    if (piece.getColor() == PieceColor.RED) {
                        application.changeSquareState(piece.getX(), piece.getY(), Color.RED, true);
                    } else {
                        application.changeSquareState(piece.getX(), piece.getY(), Color.BLUE, true);

                    }
                }
            }
        }
    }

    public boolean isStarted() {
        return started;
    }

    public void endGame() {
        if (singleplayer) {
            this.started = false;
            this.board = null;
            this.player.setCurrentScore(0);
            this.player.setTurn(false);
            this.opponent = null;
            this.level = 1;
            application.clearBoard();
            application.setScoreBoard();
        } else {
            this.started = false;
            this.board = null;
            this.player.setCurrentScore(0);
            this.opponent = null;
            this.level = 1;
            application.clearBoard();
            application.updateOpponent(opponent);
            application.setScoreBoard();

            try {
                if (session.isOpen()) {
                    session.getBasicRemote().sendText("ENDGAME");
                    session.close();
                }
            } catch (IOException e) {
                logger.info(e.getMessage());
            }
        }

    }

    public boolean checkPlayerTurn() {
        if (player.isTurn()) {
            return true;
        }
        application.alertError("Its " + opponent.getUsername() + "'s " + "turn");
        return false;
    }

    private void sendSocketMessage(Object object, TetrisSocketClientOperation operation) {
        try {
            TetrisClientSocketMessage wsMessage = new TetrisClientSocketMessage();
            wsMessage.setOperation(operation);
            wsMessage.setContent(gson.toJson(object));
            session.getBasicRemote().sendText(gson.toJson(wsMessage));
        } catch (IOException e) {
            logger.info(e.getMessage());
        }
    }
}
