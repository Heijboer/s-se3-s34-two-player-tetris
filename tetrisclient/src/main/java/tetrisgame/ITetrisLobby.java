package tetrisgame;

import tetrisgame.model.Player;

public interface ITetrisLobby {

    Player login(String username, String password);

    boolean registerUser(String username, String password);
}
