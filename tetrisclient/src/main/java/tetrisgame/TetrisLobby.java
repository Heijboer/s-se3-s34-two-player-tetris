package tetrisgame;

import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tetrisgame.model.Player;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class TetrisLobby implements ITetrisLobby{

    Logger logger = LoggerFactory.getLogger(TetrisLobby.class);
    Gson gson = new Gson();

    private static final String URL = "http://localhost:8090/tetrislobby";

    public boolean registerUser(String username, String password) {

        boolean registered = false;

        JSONObject jo = new JSONObject();
        jo.put("username", username);
        jo.put("password", password);

        final String query = URL + "/register";

        HttpPost httpPost = new HttpPost(query);
        httpPost.addHeader("content-type", "application/json");

        try {
            StringEntity params = new StringEntity(jo.toJSONString());
            httpPost.setEntity(params);
        } catch (UnsupportedEncodingException ex) {
            ex.getStackTrace();
        }

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpPost)) {
            HttpEntity entity = response.getEntity();
            final String entityString = EntityUtils.toString(entity);
            registered = Boolean.parseBoolean(entityString);
        } catch (UnsupportedEncodingException ex) {
            logger.info(ex.getMessage());
        } catch (IOException e) {
            logger.info(e.getMessage());
        }

        return registered;
    }

    public Player login(String username, String password) {

        Player player = null;

        JSONObject jo = new JSONObject();
        jo.put("username", username);
        jo.put("password", password);

        final String query = URL + "/login";

        HttpPost httpPost = new HttpPost(query);
        httpPost.addHeader("content-type", "application/json");

        try {
            StringEntity params = new StringEntity(jo.toJSONString());
            httpPost.setEntity(params);
        } catch (UnsupportedEncodingException ex) {
            ex.getStackTrace();
        }

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(httpPost)) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                final String entityString = EntityUtils.toString(entity);
                player = gson.fromJson(entityString, Player.class);
            }
        } catch (UnsupportedEncodingException ex) {
            logger.info(ex.getMessage());
        } catch (IOException e) {
            logger.info(e.getMessage());
        }

        return player;
    }
}
