package tetrisgame;

import tetrisgui.ITetrisGUI;

public interface ITetrisGame {
    void moveDown();

    void moveRight();

    void moveLeft();

    boolean isStarted();

    void startNewGame(ITetrisGUI application, boolean singleplayer);

    void endGame();

    void registerPlayer(String username);
}
