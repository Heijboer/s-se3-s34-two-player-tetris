package tetrisgame.model;

import java.util.Arrays;

public class Board {

    private static final int WIDTH = 10;
    private static final int HEIGHT = 20;

    private int level;
    Piece[][] cells = new Piece[HEIGHT][WIDTH];
    Piece currentPiece;

    public Piece[][] getCells() {
        return cells;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setCurrentPiece(Piece piece) {
        this.currentPiece = piece;
        cells[piece.getY()][piece.getX()] = currentPiece;
    }

    public boolean moveDown() {
        if (canCurrentPieceMoveDown()) {
            cells[currentPiece.getY()][currentPiece.getX()] = null;
            cells[currentPiece.getY() + 1][currentPiece.getX()] = currentPiece;
            currentPiece.setY(currentPiece.getY() + 1);
            return true;
        } else {
            this.currentPiece.setPlaced(true);
            return false;
        }
    }

    public void moveDown(Piece piece) {
        if (canPieceMoveDown(piece)) {
            cells[piece.getY()][piece.getX()] = null;
            cells[piece.getY() + 1][piece.getX()] = piece;
            piece.setY(piece.getY() + 1);
        } else {
            this.currentPiece.setPlaced(true);
        }
    }

    public boolean moveLeft() {
        if (canCurrentPieceMoveLeft()) {
            cells[currentPiece.getY()][currentPiece.getX()] = null;
            cells[currentPiece.getY()][currentPiece.getX() - 1] = currentPiece;
            currentPiece.setX(currentPiece.getX() - 1);
            return true;
        } else {
            return false;
        }
    }

    public boolean moveRight() {
        if (canCurrentPieceMoveRight()) {
            cells[currentPiece.getY()][currentPiece.getX()] = null;
            cells[currentPiece.getY()][currentPiece.getX() + 1] = currentPiece;
            currentPiece.setX(currentPiece.getX() + 1);
            return true;
        } else {
            return false;
        }
    }

    public boolean canPieceMoveDown(Piece piece) {
        return piece.getY() + 1 <= HEIGHT - 1 && cells[piece.getY() + 1][piece.getX()] == null;
    }

    public boolean canCurrentPieceMoveDown() {
        return currentPiece.getY() + 1 <= HEIGHT - 1 && cells[currentPiece.getY() + 1][currentPiece.getX()] == null;
    }

    public boolean canCurrentPieceMoveLeft() {
        return currentPiece.getX() - 1 >= 0 && cells[currentPiece.getY()][currentPiece.getX() - 1] == null;
    }

    public boolean canCurrentPieceMoveRight() {
        return currentPiece.getX() + 1 <= WIDTH - 1 && cells[currentPiece.getY()][currentPiece.getX() + 1] == null;
    }

    public int clearFullRows() {
        int rowsCleared = 0;
        for (Piece[] row : cells) {
            int pieceCount = 0;
            for (Piece cell : row) {
                if (cell != null) {
                    pieceCount++;
                    if (pieceCount == 10) {
                        rowsCleared++;
                        Arrays.fill(row, null);
                        moveAllPiecesDown(cell.getY());
                        level++;
                    }
                }
            }
        }
        return calculateScore(rowsCleared, this.level);
    }

    public boolean checkLostGame() {
        return currentPiece.getY() == 0 && currentPiece.isPlaced();
    }

    private void moveAllPiecesDown(int yRow) {
        for (int y = yRow; y >= 0; y--) {
            for (int x = 0; x < WIDTH; x++) {
                if (cells[y][x] != null && cells[y][x].getY() < yRow) {
                    moveDown(cells[y][x]);
                }
            }
        }
    }

    private int calculateScore(int rowsCleared, int level) {
        int score = 0;
        switch (rowsCleared) {
            case 1:
                return 40 * level;
            case 2:
                return 100 * level;
            case 3:
                return 300 * level;
            case 4:
                return 800 * level;
            default:
                return score;
        }
    }

    // Below methods are for testing and debugging purposes

    public void printBoard() {
        // Loop through all rows
        for (Piece[] row : cells)

            // converting each row as string
            // and then printing in a separate line
            System.out.println(Arrays.toString(row));
    }

    public void setPiece(Piece piece) {
        cells[piece.getY()][piece.getX()] = piece;
    }

    // sets a new piece in the top middle of the screen
    public void setCurrentPiece() {
        this.currentPiece = new Piece(0, 4, PieceColor.RED);
        cells[currentPiece.getY()][currentPiece.getX()] = currentPiece;
    }
}
