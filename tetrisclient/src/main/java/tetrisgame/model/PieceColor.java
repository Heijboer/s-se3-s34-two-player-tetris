package tetrisgame.model;

public enum PieceColor {
    RED,
    BLUE
}
