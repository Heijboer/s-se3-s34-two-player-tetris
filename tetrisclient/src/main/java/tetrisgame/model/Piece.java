package tetrisgame.model;


public class Piece {

    private int x;
    private int y;

    private PieceColor color;
    private boolean placed;

    public Piece(int y, int x) {
        this.y = y;
        this.x = x;
    }

    public Piece(int y, int x, PieceColor color) {
        this.y = y;
        this.x = x;
        this.color = color;
    }

    public PieceColor getColor() {
        return color;
    }

    public void setColor(PieceColor color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isPlaced() {
        return placed;
    }

    public void setPlaced(boolean placed) {
        this.placed = placed;
    }

    public String toString() {
        return getClass().getSimpleName() + "[x=" + x + ",y=" + y + "]";
    }
}
