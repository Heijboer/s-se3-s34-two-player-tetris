package networking.messaging;

public enum TetrisSocketServerOperation {
    SETPLAYER,
    SETOPPONENT,
    UPDATEPLAYER,
    UPDATEOPPONENT,
    UPDATEBOARD,
    UPDATELEVEL,
    SETWINNER,
    SETLOSER,
    SETDRAW
}
