package networking.messaging;

public enum  TetrisSocketClientOperation {
    REGISTER,
    MOVELEFT,
    MOVERIGHT,
    MOVEDOWN,
    ENDGAME
}
