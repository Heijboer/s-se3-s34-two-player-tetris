package networking;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;

@ClientEndpoint
public class TetrisGameClientSocket {

    private static final Logger logger = LoggerFactory.getLogger(TetrisGameClientSocket.class);

    private static MessageHandler messageHandler;
    private Gson gson = new Gson();

    @OnOpen
    public void onWebSocketConnect() {
        logger.info("[Connected]");
    }

    @OnMessage
    public void onWebSocketText(String message) {
        logger.info("[Received]: {}", message);

        if (messageHandler != null) {
            messageHandler.handleMessage(message);
        }
    }

    @OnClose
    public void onWebSocketClose(CloseReason reason) {
        logger.info("[Closed]: {}", reason);
    }

    @OnError
    public void onWebSocketError(Throwable cause) {
        logger.info("[ERROR]: {}", cause.getMessage());
    }

    public static void addMessageHandler(MessageHandler msgHandler) {
        messageHandler = msgHandler;
    }
}

