package tetrisgui;

import javafx.application.Application;

import static javafx.application.Application.launch;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import tetrisgame.ITetrisGame;
import tetrisgame.ITetrisLobby;
import tetrisgame.TetrisGame;
import tetrisgame.TetrisLobby;
import tetrisgame.model.Player;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TetrisApplication extends Application implements ITetrisGUI {

    ITetrisGame tetrisGame;
    ITetrisLobby tetrisLobby;
    Stage window;
    Scene loginScene;
    Scene menuScene;
    Scene gameScene;
    Player player;
    Player opponent;
    private static final int START_TIME = 3000;
    int level = 1;
    boolean singleplayer;

    // Constants to define size of GUI elements
    private static final int BORDERSIZE = 10; // Size of borders in pixels
    private static final int AREAWIDTH = 350; // Width of area in pixels
    private static final int AREAHEIGHT = AREAWIDTH * 2; // Height of area in pixels
    private static final int SQUAREWIDTH = 36; // Width of single square in pixels
    private static final int SQUAREHEIGHT = 36; // Height of single square in pixels
    private static final int LABELWIDTH = 180; // Width of button

    // Constants to define number of squares horizontal and vertical
    private static final int NRSQUARESVERTICAL = 20;
    private static final int NRSQUARESHORIZONTAL = 10;

    private static final String FONTTYPE = "Arial";

    // Multithreading
    ScheduledExecutorService executor;
    ScheduledFuture<?> futureTask;
    private Runnable moveDownRunnable;

    // Score
    Label playerScoreLabel;
    Label opponentScoreLabel;
    Label labelTetris;
    Label levelLabel;

    // Squares for the playfield
    private Rectangle[][] playfieldSquares;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.window = primaryStage;

        // Login screen
        BorderPane loginBorder = new BorderPane();
        loginBorder.setCenter(addLoginVBox());
        this.loginScene = new Scene(loginBorder, 500, 500);

        // Menu screen
        BorderPane menuBorder = new BorderPane();
        menuBorder.setCenter(addMenuVBox());
        this.menuScene = new Scene(menuBorder, 500, 500);

        // Game screen
        // Define grid pane
        GridPane grid;
        grid = new GridPane();
        grid.setHgap(BORDERSIZE);
        grid.setVgap(BORDERSIZE);
        grid.setPadding(new Insets(BORDERSIZE, BORDERSIZE, BORDERSIZE, BORDERSIZE));

        // Create the scene and add the grid pane
        Group root = new Group();
        this.gameScene = new Scene(root, AREAWIDTH + (double)LABELWIDTH + 3 * BORDERSIZE, 2 * AREAHEIGHT + 2 * (double)BORDERSIZE + 65, Color.DARKSLATEGRAY);
        root.getChildren().add(grid);

        // Label for player's name
        labelTetris = new Label("Two Player Tetris");
        labelTetris.setMinWidth(AREAWIDTH);
        grid.add(labelTetris, 0, 33, 1, 2);

        // playfield, 10, 20
        Rectangle playfield = new Rectangle(BORDERSIZE, 3 * (double) BORDERSIZE, AREAWIDTH, AREAHEIGHT);
        playfield.setFill(Color.BLACK);
        root.getChildren().add(playfield);

        playfieldSquares = new Rectangle[NRSQUARESHORIZONTAL][NRSQUARESVERTICAL];
        for (int i = 0; i < NRSQUARESHORIZONTAL; i++) {
            for (int j = 0; j < NRSQUARESVERTICAL; j++) {
                double x = playfield.getX() + i * ((double) AREAWIDTH / NRSQUARESHORIZONTAL) + 2;
                double y = playfield.getY() + j * ((double) AREAHEIGHT / NRSQUARESVERTICAL) + 2;
                Rectangle rectangle = new Rectangle(x, y, SQUAREWIDTH, SQUAREHEIGHT);
                rectangle.setStroke(Color.BLACK);
                rectangle.setFill(Color.LIGHTBLUE);
                rectangle.setVisible(true);
                playfieldSquares[i][j] = rectangle;
                root.getChildren().add(rectangle);
            }
        }

        // Text field to set the player's name
        Label scoreTitle = new Label("Score:");
        grid.add(scoreTitle, 1, 2, 1, 2);

        // Label for player score
        playerScoreLabel = new Label("");
        grid.add(playerScoreLabel, 1, 5, 1, 2);

        // Label for score
        opponentScoreLabel = new Label("");
        grid.add(opponentScoreLabel, 1, 6, 1, 2);

        // Label for current level
        levelLabel = new Label("");
        grid.add(levelLabel, 1, 10, 1, 2);


        // Set font for all labeled objects
        for (Node n : grid.getChildren()) {
            if (n instanceof Labeled) {
                ((Labeled) n).setFont(new Font(FONTTYPE, 13));
                ((Labeled) n).setTextFill(Color.WHITE);
            }
        }

        scoreTitle.setFont(new Font(24.00));

        scoreTitle.setFont(new Font(18.00));


        // Key input events
        this.gameScene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if (key.getCode() == KeyCode.DOWN) {
                tetrisGame.moveDown();
            }
        });

        // Key input events
        this.gameScene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if (key.getCode() == KeyCode.RIGHT) {
                tetrisGame.moveRight();

            }
        });

        // Key input events
        this.gameScene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if (key.getCode() == KeyCode.LEFT) {
                tetrisGame.moveLeft();
            }
        });

        // Define title and assign the scene for main window
        this.window.setScene(loginScene);
        this.window.setTitle("Tetris");
        this.window.show();

        tetrisGame = new TetrisGame();
        tetrisLobby = new TetrisLobby();

        // Game loop

        executor = Executors.newScheduledThreadPool(1);

        moveDownRunnable = () -> {
            if (tetrisGame.isStarted() && singleplayer) {
                tetrisGame.moveDown();
            }
        };
    }

    public void changeReadInterval(long time) {
        if (time > 0) {
            if (futureTask != null) {
                futureTask.cancel(true);
            }
            futureTask = executor.scheduleAtFixedRate(moveDownRunnable, START_TIME  , time, TimeUnit.MILLISECONDS);
        }
    }

    private VBox addLoginVBox() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);
        vbox.setAlignment(Pos.TOP_CENTER);

        // Login
        Text title = new Text("Login");
        title.setFont(Font.font(FONTTYPE, FontWeight.BOLD, 20));

        Label usernameLbl = new Label("User Name:");

        TextField usernameField = new TextField();
        usernameField.setMaxWidth(150);

        Label passwordLbl = new Label("Password:");

        PasswordField passwordField = new PasswordField();
        passwordField.setMaxWidth(150);

        Button loginButton = new Button("Sign in");
        loginButton.setOnAction(e -> loginEvent(usernameField.getText(), passwordField.getText()));

        // Register
        Text title2 = new Text("Register");
        title2.setFont(Font.font(FONTTYPE, FontWeight.BOLD, 20));

        Label usernameLbl2 = new Label("User Name:");

        TextField usernameField2 = new TextField();
        usernameField2.setMaxWidth(150);

        Label passwordLbl2 = new Label("Password:");

        PasswordField passwordField2 = new PasswordField();
        passwordField2.setMaxWidth(150);

        Button registerButton = new Button("Register");
        registerButton.setOnAction(e -> registerEvent(usernameField2.getText(), passwordField2.getText()));

        vbox.getChildren().addAll(title, usernameLbl, usernameField, passwordLbl, passwordField, loginButton,
                title2, usernameLbl2, usernameField2, passwordLbl2, passwordField2, registerButton);

        return vbox;
    }

    private VBox addMenuVBox() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);
        vbox.setAlignment(Pos.CENTER);

        Text title = new Text("Menu");
        title.setFont(Font.font(FONTTYPE, FontWeight.BOLD, 20));

        Button singleplayerButton = new Button("Singleplayer");
        singleplayerButton.setOnAction(e -> startNewGame(true));
        singleplayerButton.setMaxWidth(150);
        singleplayerButton.setMinHeight(50);

        Button multiplayerButton = new Button("Multiplayer");
        multiplayerButton.setOnAction(e -> startNewGame(false));
        multiplayerButton.setMaxWidth(150);
        multiplayerButton.setMinHeight(50);

        vbox.getChildren().addAll(title, singleplayerButton, multiplayerButton);

        return vbox;
    }

    private void startNewGame(boolean singleplayer) {
        this.singleplayer = singleplayer;
        tetrisGame.startNewGame(this, singleplayer);
        this.window.setScene(gameScene);
    }

    public void changeSquareState(final int posX, final int posY, final boolean state) {
        Platform.runLater(() -> {
            Rectangle square = playfieldSquares[posX][posY];

            if (state) {
                if (player.isTurn()) {
                    square.setFill(Color.RED);
                } else if (opponent.isTurn()) {
                    square.setFill(Color.BLUE);
                }
            } else {
                square.setFill(Color.LIGHTBLUE);
            }
        });
    }

    public void changeSquareState(final int posX, final int posY, Color color, final boolean state) {
        Platform.runLater(() -> {
            Rectangle square = playfieldSquares[posX][posY];

            if (state) {
                square.setFill(color);
            } else {
                square.setFill(Color.LIGHTBLUE);
            }
        });
    }

    public void clearBoard() {
        Platform.runLater(() -> {
            for (Rectangle[] row : playfieldSquares) {
                for (Rectangle rectangle : row) {
                    rectangle.setFill(Color.LIGHTBLUE);
                }
            }
        });

    }

    private void loginEvent(String username, String password) {
        Player loggedInPlayer = tetrisLobby.login(username, password);

        if (loggedInPlayer != null) {
            this.tetrisGame.registerPlayer(username);
            this.window.setScene(menuScene);
        } else {
            alertError("Wrong username or password");
        }
    }

    private void registerEvent(String username, String password) {
        if (username.length() > 20) {
            alertError("Username can not be longer than 20 characters");
        } else if (password.length() > 40) {
            alertError("Password can not be longer than 40 characters");
        } else {
            if (!tetrisLobby.registerUser(username, password)) {
                alertError("Username already exists");
            } else {
                alertConfirmation("Successfully registered");
            }
        }
    }

    public void alertConfirmation(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message, ButtonType.OK);
        alert.showAndWait();
    }

    public void alertError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.showAndWait();
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
        this.alertConfirmation("Your opponent is " + opponent.getUsername());
    }

    public void setScoreBoard() {
        Platform.runLater(() -> {
            if (player != null) {
                playerScoreLabel.setText(player.getUsername() + ": " + player.getCurrentScore());
            } else {
                playerScoreLabel.setText("");
            }
            if (opponent != null) {
                opponentScoreLabel.setText(opponent.getUsername() + ": " + opponent.getCurrentScore());
            } else {
                opponentScoreLabel.setText("");
            }
        });
    }

    public void updateLevel(int updatedLevel) {
        Platform.runLater(() -> {
            level = updatedLevel;
            levelLabel.setText("Level: " + updatedLevel);
            changeReadInterval(1000 / ((long)Math.pow(level, 1.05)));
        });
    }

    public void setWon(String playerName) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, playerName + " WON! Returning to main menu..", ButtonType.OK);
        alert.showAndWait();

        tetrisGame.endGame();
        this.window.setScene(menuScene);
    }

    public void setDraw() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "DRAW! returning to main menu..", ButtonType.OK);
        alert.showAndWait();

        tetrisGame.endGame();
        this.window.setScene(menuScene);
    }

    public void updatePlayer(Player player) {
        this.player = player;
    }

    public void updateOpponent(Player opponent) {
        this.opponent = opponent;
    }
}
