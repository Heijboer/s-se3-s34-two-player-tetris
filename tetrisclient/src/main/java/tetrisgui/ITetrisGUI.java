package tetrisgui;

import javafx.scene.paint.Color;
import tetrisgame.model.Player;

public interface ITetrisGUI {

    void setPlayer(Player player);

    void setOpponent(Player opponent);

    void updatePlayer(Player player);

    void updateOpponent(Player opponent);

    void setScoreBoard();

    void changeSquareState(int xPos, int yPos, boolean bool);

    void changeSquareState(int xPos, int yPos, Color color, boolean bool);

    void clearBoard();

    void updateLevel(int level);

    void setWon(String username);

    void setDraw();

    void alertError(String message);

    void alertConfirmation(String message);
}
